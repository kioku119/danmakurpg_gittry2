﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SoundUtility
{
    public static class SoundUtils
    {
        //#TODO if this gets slow looking into disable and reenable.
        const int maxSources = 50;
        public static float s_sfxVolume = 1f;
        private static List<AudioSource> audioSources = new List<AudioSource>(maxSources);
        private static GameObject audioObject = new GameObject();

        static int currentSource = 0;

        public static void UpdateSFXVolume()
        {
            s_sfxVolume = PlayerPrefs.GetFloat("Sound Effect Volume Multiplier", 1f);
        }

        public static void CreateSources()
        {
            for (int i = 0; i < maxSources; ++i)
            {
                AudioSource aSource = audioObject.AddComponent<AudioSource>();
                aSource.rolloffMode = AudioRolloffMode.Linear;
                aSource.volume = s_sfxVolume;
                audioSources.Add(aSource);

            }
        }

        public static void PlaySoundEffect(AudioClip clip)
        {
            if (!DarkTonic.CoreGameKit.LevelSettings.IsGameOver)
            {
                if (audioSources.Count == 0)
                {
                    CreateSources();
                }
                AudioSource source = audioSources[currentSource];
                source.volume = s_sfxVolume;
                source.PlayOneShot(clip);

                ++currentSource;
                if (currentSource > (maxSources - 1))
                {
                    currentSource = 0;
                }
            }
        }
    }

    /*public static AudioSource PlaySoundEffect(AudioClip clip)
        {
            Vector3 pos = new Vector3(0f, 0f, 0f);
            GameObject tempGO = new GameObject("TempAudio");
            tempGO.transform.position = pos;
            AudioSource aSource = tempGO.AddComponent<AudioSource>();
            aSource.rolloffMode = AudioRolloffMode.Linear;
            aSource.clip = clip;
            aSource.volume = s_sfxVolume;

            aSource.Play();
            Object.Destroy(tempGO, clip.length);
            return aSource;
        }*/
}
