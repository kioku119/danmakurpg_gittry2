using UnityEngine;
using System.Collections.Generic;
using DarkTonic.CoreGameKit;
using SoundUtility;

[AddComponentMenu("Dark Tonic/Core GameKit/Listeners/Enemy Killable Listener")]

public class NormalEnemyDamageListener : KillableListener
{
    public AudioClip m_enemyDamageSound;
    public AudioClip m_enemyDeathSound;

	// if you need more than one Listener for of each type (KillableListener etc), create subclasses like this, inheriting from KillableListener
	public override void Despawning(TriggeredSpawner.EventType eType) {
		base.Despawning(eType);

	    if (m_enemyDeathSound != null)
	    {
	        SoundUtils.PlaySoundEffect(m_enemyDeathSound);
	    }

        // your code here.
        Debug.Log("KillableListenerSubclass (on MainCamera): Played died! Take some action");
	}
	
	public override void TakingDamage(int pointsDamage, Killable enemyHitBy)
	{
		base.TakingDamage(pointsDamage, enemyHitBy);

	    if (m_enemyDamageSound != null)
	    {
	        SoundUtils.PlaySoundEffect(m_enemyDamageSound);
        }
        // your code here.
    }

	public override void DamagePrefabSpawned(Transform damagePrefab) {
		base.DamagePrefabSpawned(damagePrefab);
		
		// your code here.
	}
	
	public override void DamagePrefabFailedToSpawn(Transform damagePrefab) {
		base.DamagePrefabFailedToSpawn(damagePrefab);
		
		// your code here.  
	}
	
	public override void DeathPrefabSpawned(Transform deathPrefab) {
		base.DeathPrefabSpawned(deathPrefab);
		
		// your code here.
		Debug.Log("Death prefab spawned for " + this.sourceKillableName);
	}
	
	public override void DeathPrefabFailedToSpawn(Transform deathPrefab) {
		base.DeathPrefabFailedToSpawn(deathPrefab);
		
		// your code here.  
	}

	public override void ModifyingDeathWorldVariables(List<WorldVariableModifier> variableModifiers) {
		base.ModifyingDeathWorldVariables(variableModifiers);
		
		// your code here.
		Debug.Log("Modifying world variations for " + this.sourceKillableName + " destruction");
	}

    public override void Spawned(Killable newKillable)
    {
       // m_enemyDamageSound = newKillable.GetComponentInParent<>();
    }
}
    