﻿using System;
using System.Collections;
using System.Collections.Generic;
using DarkTonic.CoreGameKit;
using SoundUtility;
using UnityEngine;

public class PlayerDamageListener : KillableListener
{

    public AudioClip m_playerDamageSound;
    public AudioClip m_playerDeathSound;

    // if you need more than one Listener for of each type (KillableListener etc), create subclasses like this, inheriting from KillableListener
    public override void Despawning(TriggeredSpawner.EventType eType)
    {
        base.Despawning(eType);

        if (m_playerDeathSound != null)
        {
            SoundUtils.PlaySoundEffect(m_playerDeathSound);
        }

        // your code here.
        Debug.Log("KillableListenerSubclass (on MainCamera): Played died! Take some action");
    }

    public override void TakingDamage(int pointsDamage, Killable enemyHitBy)
    {
        var hp = WorldVariableTracker.GetWorldVariable("Health");
        int currHp = hp.CurrentIntValue;
        hp.SetIntValueIfAllowed(Mathf.Max(currHp - pointsDamage, 0));

        base.TakingDamage(pointsDamage, enemyHitBy);

        if (m_playerDamageSound != null)
        {
            SoundUtils.PlaySoundEffect(m_playerDamageSound);
        }
        // your code here.
    }

    public override void DamagePrefabSpawned(Transform damagePrefab)
    {
        base.DamagePrefabSpawned(damagePrefab);

        // your code here.
    }

    public override void DamagePrefabFailedToSpawn(Transform damagePrefab)
    {
        base.DamagePrefabFailedToSpawn(damagePrefab);

        // your code here.  
    }

    public override void DeathPrefabSpawned(Transform deathPrefab)
    {
        base.DeathPrefabSpawned(deathPrefab);

        // your code here.
        Debug.Log("Death prefab spawned for " + this.sourceKillableName);
    }

    public override void DeathPrefabFailedToSpawn(Transform deathPrefab)
    {
        base.DeathPrefabFailedToSpawn(deathPrefab);

        // your code here.  
    }

    public override void ModifyingDeathWorldVariables(List<WorldVariableModifier> variableModifiers)
    {
        base.ModifyingDeathWorldVariables(variableModifiers);

        // your code here.
        Debug.Log("Modifying world variations for " + this.sourceKillableName + " destruction");
    }

    public override void Spawned(Killable newKillable)
    {
        var hp = WorldVariableTracker.GetWorldVariable("Health");
        int currHp = newKillable.hitPoints.Value;
        hp.SetIntValueIfAllowed(currHp);
    }
}
