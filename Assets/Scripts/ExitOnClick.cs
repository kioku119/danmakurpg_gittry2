﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//test: example comment
//test test: more example comments
//LOLOLOLOLOLOLOLOLOLOLOLOLOL
public class ExitOnClick : MonoBehaviour {

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
