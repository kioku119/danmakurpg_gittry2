﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataCache : MonoBehaviour
{
    private static Camera s_mainCamera;

    public static Camera GetMainCamera()
    {
        if (!s_mainCamera)
        {
            s_mainCamera = Camera.main;
        }

        return s_mainCamera;
    }
}
