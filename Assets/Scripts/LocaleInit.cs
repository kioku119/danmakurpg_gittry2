﻿#define DEVELOPMENT
using System.Collections;
using System.Collections.Generic;
using Localization;
using UnityEngine;

public class LocaleInit : MonoBehaviour
{
    private static bool s_initialized = false;

    void Awake()
    {
        #if (DEVELOPMENT)
        {
            if (!s_initialized)
            {
                DefaultInitLocalization();
            }
        }
        #endif
    }

    //#TODO: REMOVE IN RELEASE
    void DefaultInitLocalization()
    {
            if (!LocalizationManager.instance)
            {
                GameObject go = new GameObject();
                LocalizationManager.instance = go.AddComponent<LocalizationManager>();
            }
            if (!LocalizationManager.instance.GetIsReady())
            {
                string locString = PlayerPrefs.GetString("LanguageKey", "localizedText_en.json");
                LocalizationManager.instance.LoadLocalizedText(locString);
            }

            s_initialized = true;
    }
}
