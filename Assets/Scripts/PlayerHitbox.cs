﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitbox : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    CircleCollider2D c2d = GetComponentsInParent(typeof(CircleCollider2D))[0] as CircleCollider2D;
	    float oldRadius = c2d.radius * 2f;
	    transform.SetLocalScale(oldRadius, oldRadius, oldRadius);
	    UbhPlayer play = GetComponentsInParent(typeof(UbhPlayer))[0] as UbhPlayer;
	    Vector3 pos = play.transform.position;
	    transform.position = pos;
	}
}
