﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Localization;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public struct LanguageInfo
{
    public string languageName;
    public string languageKey;
}
    
public class UIDropdown : MonoBehaviour {
    [Tooltip("Add items (gameobjects) to the dropdown which has the given tag.")]
    public string populateByTag;

    public static List<LanguageInfo> languagesToFilePaths;
    public const string localizationFilePrefix = "localizedText_";
    public const string localizationFileExtension = ".json";

    Dropdown m_dropdown;

    void Awake()
    {
        languagesToFilePaths = new List<LanguageInfo>(10);

        LanguageInfo eng;
        eng.languageName = "English";
        eng.languageKey = "en";

        LanguageInfo al;
        al.languageName = "Alien";
        al.languageKey = "al";

        languagesToFilePaths.Add(eng);
        languagesToFilePaths.Add(al);
    }

    void Start()
    {
        m_dropdown = gameObject.GetComponent<Dropdown>();
        m_dropdown.ClearOptions();

        foreach (LanguageInfo st in languagesToFilePaths)
        {
            AddItemToDropdown(st.languageName);
        }

        m_dropdown.onValueChanged.AddListener(delegate
        {
            string selected = m_dropdown.options[m_dropdown.value].text;
            SelectOption(m_dropdown.value);
        });

        SelectCurrentLocale();
        m_dropdown.RefreshShownValue();
    }

    public void SelectOption(int opt)
    {
        LanguageInfo langSelected = languagesToFilePaths[opt];
        string fileName = localizationFilePrefix + langSelected.languageKey + localizationFileExtension;
        PlayerPrefs.SetString("LanguageKey", fileName);

        LocalizationManager.instance.LoadLocalizedText(fileName);
        m_dropdown.RefreshShownValue();
        LocalizationManager.instance.OnUpdateLocalizationText();
    }

    public void SelectCurrentLocale()
    {
        string currentLocale = GetLocaleForPath(PlayerPrefs.GetString("LanguageKey", "localizedText_en.json"));
        for (int index = 0; index < m_dropdown.options.Count; index++)
        {
            Dropdown.OptionData option = m_dropdown.options[index];
            string selected = option.text;
            if (selected == currentLocale)
            {
                m_dropdown.value = index;
                m_dropdown.RefreshShownValue();
                return;
            }
        }
    }

    public string GetLocaleForPath(string path)
    {
        foreach(LanguageInfo li in languagesToFilePaths)
        {
            string fileName = localizationFilePrefix + li.languageKey + localizationFileExtension;
            if (path == fileName)
            {
                return li.languageName;
            }
        }
        return "";
    }

    public void AddItemToDropdown(string item)
    {
        m_dropdown.options.Add(new Dropdown.OptionData(item));
    }
}
