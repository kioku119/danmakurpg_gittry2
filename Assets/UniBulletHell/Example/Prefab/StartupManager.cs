﻿using Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartupManager : MonoBehaviour
{
    void Start()
    {
        string languageKey = PlayerPrefs.GetString("LanguageKey", "X");
        if (languageKey != "X")
        {
            LocalizationManager.instance.LoadLocalizedText(languageKey);
        }

        StartGame();
    }

    private IEnumerator LoadMenu()
    {
        while (!LocalizationManager.instance.GetIsReady())
        {
            yield return null;
        }

        SceneManager.LoadScene("Menu1");
    }

    public void StartGame()
    {
        IEnumerator startRoutine = LoadMenu();
        StartCoroutine(startRoutine);
    }
}
