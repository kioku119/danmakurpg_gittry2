﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDown : MonoBehaviour
{
    public static float s_fallSpeed = 3.5f;

    public static bool s_resetVelocity = true;
    public float m_fallSpeed = 3.5f;

    private Vector2 fallVector;
    private Rigidbody2D rigidBodyCache;
    // Use this for initialization
    void Start ()
	{
        rigidBodyCache = GetComponent<Rigidbody2D>();
	    fallVector = Vector2.down * m_fallSpeed;
	}

    void Update()
    {
        if (s_resetVelocity)
        { 
            m_fallSpeed = s_fallSpeed;
            rigidBodyCache.velocity = fallVector;
        }
    }
}
