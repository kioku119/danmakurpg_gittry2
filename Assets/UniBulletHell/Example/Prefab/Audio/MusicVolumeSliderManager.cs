﻿#define DEVELOPMENT
using DarkTonic.CoreGameKit;
using System.Collections;
using System.Collections.Generic;
using Localization;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeSliderManager : MonoBehaviour
{
    public static MusicVolumeSliderManager i;
    public Slider volumeSlider = null;

    void Awake()
    {

        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(gameObject);
 
        }
        else Destroy(this);

        i.volumeSlider.value = PlayerPrefs.GetFloat("Music Volume Multiplier", 1f);
        i.volumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChange(); });
    }

    public void OnMusicVolumeChange()
    {
        PlayerPrefs.SetFloat("Music Volume Multiplier", i.volumeSlider.value);
    }
}
