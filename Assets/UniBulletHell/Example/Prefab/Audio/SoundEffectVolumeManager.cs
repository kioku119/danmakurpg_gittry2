﻿using System.Collections;
using System.Collections.Generic;
using SoundUtility;
using UnityEngine;
using UnityEngine.UI;

public class SoundEffectVolumeManager : MonoBehaviour {
    public static SoundEffectVolumeManager i;
    public Slider volumeSlider = null;

    void Awake()
    {
        if (i == null)
        {
            i = this;
            DontDestroyOnLoad(gameObject);

        }
        else Destroy(this);

        i.volumeSlider.value = PlayerPrefs.GetFloat("Sound Effect Volume Multiplier", 1f);
        i.volumeSlider.onValueChanged.AddListener(delegate { OnSFXVolumeChange(); });
    }

    public void OnSFXVolumeChange()
    {
        PlayerPrefs.SetFloat("Sound Effect Volume Multiplier", i.volumeSlider.value);
        SoundUtils.UpdateSFXVolume();
    }
}
