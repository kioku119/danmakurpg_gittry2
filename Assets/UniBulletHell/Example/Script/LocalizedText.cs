﻿#define DEVELOPMENT
using System.Collections;
using System.Collections.Generic;
using Localization;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string key;

    // Use this for initialization
    void Start()
    {
        Text text = GetComponent<Text>();
        text.text = LocalizationManager.instance.GetLocalizedValue(key);
        LocalizationManager.instance.LocalizationUpdate += new LocalizationManager.LocalizationUpdateHandler(UpdateText);
    }

    public void UpdateText()
    {
        Text text = GetComponent<Text>();
        text.text = LocalizationManager.instance.GetLocalizedValue(key);
    }
}