﻿using System.Collections.Generic;
using UnityEngine;
using DarkTonic.CoreGameKit;

// ReSharper disable once CheckNamespace

/// <summary>
/// This class is used to listen to key events in a Killable. Alwa  ys make a subclass so you can have different Listeners for different Killables.
/// </summary>
// ReSharper disable once CheckNamespace
[AddComponentMenu("Dark Tonic/Core GameKit/Listeners/Bullet Killable Listener")]
public class BulletKiller : KillableListener
{
    // if you need more than one Listener for of each type (KillableListener etc), create subclasses like this, inheriting from KillableListener
    public override void Despawning(TriggeredSpawner.EventType eType)
    {
        base.Despawning(eType);

        UbhBullet bullet = gameObject.GetComponent<UbhBullet>();
        UbhBulletManager.instance.RemoveBullet(bullet);
    }

    public override void TakingDamage(int pointsDamage, Killable enemyHitBy)
    {
        base.TakingDamage(pointsDamage, enemyHitBy);

        // your code here.
    }

    public override void DamagePrefabSpawned(Transform damagePrefab)
    {
        base.DamagePrefabSpawned(damagePrefab);

        // your code here.
    }

    public override void DamagePrefabFailedToSpawn(Transform damagePrefab)
    {
        base.DamagePrefabFailedToSpawn(damagePrefab);

        // your code here.  
    }

    public override void DeathPrefabSpawned(Transform deathPrefab)
    {
        base.DeathPrefabSpawned(deathPrefab);

        // your code here.
        Debug.Log("Death prefab spawned for " + this.sourceKillableName);
    }

    public override void DeathPrefabFailedToSpawn(Transform deathPrefab)
    {
        base.DeathPrefabFailedToSpawn(deathPrefab);

        // your code here.  
    }
}