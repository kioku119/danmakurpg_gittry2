﻿#define DEVELOPMENT

using DarkTonic.CoreGameKit;
using Localization;
using UnityEngine;
using UnityEngine.Serialization;

public class UbhGameManager : UbhMonoBehaviour
{
    public const int BASE_SCREEN_WIDTH = 600;
    public const int BASE_SCREEN_HEIGHT = 450;

    [FormerlySerializedAs("_ScaleToFit")] public bool m_scaleToFit = false;

    [SerializeField, FormerlySerializedAs("_PlayerPrefab")] private GameObject m_playerPrefab;
    [SerializeField, FormerlySerializedAs("_Score")] private UbhScore m_score;

    private void Start()
    {

#if (DEVELOPMENT)
        if (!LocalizationManager.instance)
        {
            GameObject go = new GameObject();
            LocalizationManager.instance = go.AddComponent<LocalizationManager>();
        }
        if (!LocalizationManager.instance.GetIsReady())
        {
            string locString = PlayerPrefs.GetString("LanguageKey", "localizedText_en.json");
            LocalizationManager.instance.LoadLocalizedText(locString);
        }
#endif
        
        GameStart();
    }

    private void Update()
    {
    }

    private void GameStart()
    {
        if (m_score != null)
        {
            m_score.Initialize();
        }

        CreatePlayer();
    }

    public void GameOver()
    {
        if (m_score != null)
        {
            m_score.Save();
        }
        else
        {
            // for UBH_ShotShowcase scene.
            CreatePlayer();
        }
    }

    private void CreatePlayer()
    {
        PoolBoss.Spawn(m_playerPrefab.transform, m_playerPrefab.transform.position, UbhUtil.QUATERNION_IDENTITY,
            transform);
    }
}