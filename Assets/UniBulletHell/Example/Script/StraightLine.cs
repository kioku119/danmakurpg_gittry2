﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;

public class StraightLine : MonoBehaviour
{
    BGCurve line;

    [System.Serializable]
    public class DisplacementActionInfo
    {
        //positive or negative matters
        public bool useGoalDisplacement = true;
        public float goalDisplacement;
        //If no goal length is used it will instead grow for
        //growth time.
        public float lengthGrowthTime;
        public float displacementGrowthRate = 0;

        [HideInInspector]
        public bool isDisplacementGrowthPositive;
    }

    [System.Serializable]
    public class WidthActionInfo
    {
        public bool useGoalWidth = true;
        public float goalWidth;
        //If no goal width is used it will instead grow for
        //growth time.
        public float widthGrowthTime;
        public float widthGrowthRate = 0;

        [HideInInspector]
        public bool isWidthGrowthPositive;
    }

    [System.Serializable]
    public class AngleActionInfo
    {
        public bool useGoalAngle = true;
        public float goalAngle;
        //If no goal width is used it will instead grow for
        //growth time.
        public float angleGrowthTime;
        public float angleGrowthRate = 0;

        [HideInInspector]
        public bool isAngleGrowthPositive;
    }

    //These can reset at different times so we need 3 separate ones now
    float dActionTimePassed = -1;
    float wActionTimePassed = -1;
    float aActionTimePassed = -1;

    private BGCcCollider2DBox boxCollider;
    private LineRenderer lineRenderer;

    public float displacement;
    public float width;
    public float angle;

    [SerializeField]
    private List<DisplacementActionInfo> displacementActions = new List<DisplacementActionInfo>();
    [SerializeField]
    private List<WidthActionInfo> widthActions = new List<WidthActionInfo>();
    [SerializeField]
    private List<AngleActionInfo> angleActions = new List<AngleActionInfo>();

    DisplacementActionInfo curDAction;
    int curDIndex = 0;
    WidthActionInfo curWAction;
    int curWIndex = 0;
    AngleActionInfo curAAction;
    int curAIndex = 0;

    public bool repeatDSequence = false;
    public bool repeatWSequence = false;
    public bool repeatASequence = false;

    // Use this for initialization
    void Start()
    {
        line = gameObject.GetComponent<BGCurve>();
        line[1].PositionLocal = new Vector3(0, displacement, 0);

        boxCollider = gameObject.GetComponent<BGCcCollider2DBox>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();

        boxCollider.Height = width;
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;

        gameObject.transform.Rotate(0, 0, angle);

        if(displacementActions.Count > 0)
        {
            curDAction = displacementActions[curDIndex];

            //for some reason defaulting them to true did not work.
            if (curDAction.goalDisplacement < displacement)
                curDAction.isDisplacementGrowthPositive = false;
            else
                curDAction.isDisplacementGrowthPositive = true;
        }

        if (widthActions.Count > 0)
        {
            curWAction = widthActions[curWIndex];
            if (curWAction.goalWidth < width)
                curWAction.isWidthGrowthPositive = false;
            else
                curWAction.isWidthGrowthPositive = true;
        }

        if (angleActions.Count > 0)
        {
            curAAction = angleActions[curAIndex];
            if (curAAction.goalAngle < angle)
                curAAction.isAngleGrowthPositive = false;
            else
                curAAction.isAngleGrowthPositive = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float delta = Time.deltaTime;
        if (dActionTimePassed == -1)
        {
            dActionTimePassed = 0;
        }
        else
        {
            dActionTimePassed += delta;
        }
        if (wActionTimePassed == -1)
        {
            wActionTimePassed = 0;
        }
        else
        {
            wActionTimePassed += delta;
        }
        if (aActionTimePassed == -1)
        {
            aActionTimePassed = 0;
        }
        else
        {
            aActionTimePassed += delta;
        }

        if (curDAction != null)
        {
            if (curDAction.useGoalDisplacement)
            {
                if (((curDAction.isDisplacementGrowthPositive) && (displacement < curDAction.goalDisplacement)) ||
                    ((!curDAction.isDisplacementGrowthPositive) && (displacement > curDAction.goalDisplacement)))
                {
                    float newY = line[1].PositionLocal.y + curDAction.displacementGrowthRate * delta;
                    displacement += curDAction.displacementGrowthRate * delta;
                    line[1].PositionLocal = new Vector3(0, newY, 0);
                }
                else
                {
                    ++curDIndex;
                    if (curDIndex < displacementActions.Count)
                    {
                        curDAction = displacementActions[curDIndex];
                        if (curDAction.goalDisplacement < displacement)
                            curDAction.isDisplacementGrowthPositive = false;
                        else
                            curDAction.isDisplacementGrowthPositive = true;
                        dActionTimePassed = 0;
                    }
                    else
                    {
                        if (repeatDSequence)
                        {
                            curDIndex = 0;
                            curDAction = displacementActions[curDIndex];
                            dActionTimePassed = 0;
                        }
                        else
                            curDAction = null;
                    }
                }
            }
            else if (dActionTimePassed != 0)
            {
                if (dActionTimePassed < curDAction.lengthGrowthTime)
                {
                    float newY = line[1].PositionLocal.y + curDAction.displacementGrowthRate * delta;
                    displacement += curDAction.displacementGrowthRate * delta;
                    line[1].PositionLocal = new Vector3(0, newY, 0);
                }
                else
                {
                    ++curDIndex;
                    if (curDIndex < displacementActions.Count)
                    {
                        curDAction = displacementActions[curDIndex];
                        if (curDAction.goalDisplacement < displacement)
                            curDAction.isDisplacementGrowthPositive = false;
                        else
                            curDAction.isDisplacementGrowthPositive = true;
                        dActionTimePassed = 0;
                    }
                    else
                    {
                        if (repeatDSequence)
                        {
                            curDIndex = 0;
                            curDAction = displacementActions[curDIndex];
                            dActionTimePassed = 0;
                        }
                        else
                            curDAction = null;
                    }
                }
            }
        }

        if (curWAction != null)
        {
            if (curWAction.useGoalWidth)
            {
                if (((curWAction.isWidthGrowthPositive) && (width < curWAction.goalWidth)) ||
                ((!curWAction.isWidthGrowthPositive) && (width > curWAction.goalWidth)))
                {
                    width += curWAction.widthGrowthRate * delta;

                    boxCollider.Height = width;
                    lineRenderer.startWidth = width;
                    lineRenderer.endWidth = width;
                }
                else
                {
                    ++curWIndex;
                    if (curWIndex < widthActions.Count)
                    {
                        curWAction = widthActions[curWIndex];
                        if (curWAction.goalWidth < width)
                            curWAction.isWidthGrowthPositive = false;
                        else
                            curWAction.isWidthGrowthPositive = true;
                        wActionTimePassed = 0;
                    }
                    else
                    {
                        if (repeatWSequence)
                        {
                            curWIndex = 0;
                            curWAction = widthActions[curWIndex];
                            wActionTimePassed = 0;
                        }
                        else
                            curWAction = null;
                    }
                }
            }
            else if (wActionTimePassed != 0)
            {
                if (wActionTimePassed < curWAction.widthGrowthTime)
                {
                    width += curWAction.widthGrowthRate * delta;

                    boxCollider.Height = width;
                    lineRenderer.startWidth = width;
                    lineRenderer.endWidth = width;
                }
                else
                {
                    ++curWIndex;
                    if (curWIndex < widthActions.Count)
                    {
                        curWAction = widthActions[curWIndex];
                        if (curWAction.goalWidth < width)
                            curWAction.isWidthGrowthPositive = false;
                        else
                            curWAction.isWidthGrowthPositive = true;
                        wActionTimePassed = 0;
                    }
                    else
                    {
                        if (repeatWSequence)
                        {
                            curWIndex = 0;
                            curWAction = widthActions[curWIndex];
                            wActionTimePassed = 0;
                        }
                        else
                            curWAction = null;
                    }
                }
            }
        }

        if (curAAction != null)
        {
            if (curAAction.useGoalAngle)
            {
                if (((curAAction.isAngleGrowthPositive) && (angle < curAAction.goalAngle)) ||
                ((!curAAction.isAngleGrowthPositive) && (angle > curAAction.goalAngle)))
                {
                    float angleDisplacement = curAAction.angleGrowthRate * delta;
                    angle += angleDisplacement;
                    gameObject.transform.Rotate(0, 0, angleDisplacement);

                }
                else
                {
                    ++curAIndex;
                    if (curAIndex < angleActions.Count)
                    {
                        curAAction = angleActions[curAIndex];
                        if (curAAction.goalAngle < angle)
                            curAAction.isAngleGrowthPositive = false;
                        else
                            curAAction.isAngleGrowthPositive = true;
                        aActionTimePassed = 0;
                    }
                    else
                    {
                        if (repeatASequence)
                        {
                            curAIndex = 0;
                            curAAction = angleActions[curAIndex];
                            aActionTimePassed = 0;
                        }
                        else
                            curAAction = null;
                    }
                }
            }
            else if (aActionTimePassed != 0)
            {
                if (aActionTimePassed < curAAction.angleGrowthTime)
                {
                    float angleDisplacement = curAAction.angleGrowthRate * delta;
                    angle += angleDisplacement;
                    gameObject.transform.Rotate(0, 0, angleDisplacement);
                }
                else
                {
                    ++curAIndex;
                    if (curAIndex < angleActions.Count)
                    {
                        curAAction = angleActions[curAIndex];
                        if (curAAction.goalAngle < angle)
                            curAAction.isAngleGrowthPositive = false;
                        else
                            curAAction.isAngleGrowthPositive = true;
                        aActionTimePassed = 0;
                    }
                    else
                    {
                        if(repeatASequence)
                        {
                            curAIndex = 0;
                            curAAction = angleActions[curAIndex];
                            aActionTimePassed = 0;
                        }
                        else
                            curAAction = null;
                    }
                }
            }
        }
    }
}
