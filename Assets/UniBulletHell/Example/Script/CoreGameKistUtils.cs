﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkTonic.CoreGameKit;

public class CoreGameKitUtils {

    public static void AddKillableChildCollision(GameObject gObject)
    {
        if (gObject.GetComponent<KillableChildCollision>() == null)
        {
            gObject.AddComponent<KillableChildCollision>();
        }
    }

    public static void AddKillable(GameObject gObject)
    {
        if (gObject.GetComponent<Killable>() == null)
        {
            gObject.AddComponent<Killable>();
        }
    }
}
