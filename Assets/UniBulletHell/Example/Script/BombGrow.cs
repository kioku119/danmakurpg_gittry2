﻿using System.Collections;
using System.Collections.Generic;
using Shapes2D;
using UnityEditor;
using UnityEngine;

public class BombGrow : MonoBehaviour
{
    public float growthPerSecond = 1f;
    
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	    CircleCollider2D c2d = GetComponentsInParent(typeof(CircleCollider2D))[0] as CircleCollider2D;
	    float oldRadius = c2d.radius;
	    float radiusGrowth = (growthPerSecond * Time.smoothDeltaTime);

        float fraction = (c2d.radius + radiusGrowth) / oldRadius;

	    Vector3 scale = transform.localScale * fraction;
        transform.SetLocalScale(scale.x, scale.y, scale.z);

        UbhPlayer play = GetComponentsInParent(typeof(UbhPlayer))[0] as UbhPlayer;
	    transform.position = play.transform.position;
    }
}
