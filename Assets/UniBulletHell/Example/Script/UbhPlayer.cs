﻿using DarkTonic.CoreGameKit;
using SoundUtility;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(UbhSpaceship))]
public class UbhPlayer : UbhMonoBehaviour
{
    public GameObject m_bombPrefab;
    public const string NAME_ENEMY_BULLET = "EnemyBullet";
    public const string NAME_ENEMY = "Enemy";

    private const string AXIS_HORIZONTAL = "Horizontal";
    private const string AXIS_VERTICAL = "Vertical";

    private readonly Vector2 VIEW_PORT_LEFT_BOTTOM = UbhUtil.VECTOR2_ZERO;
    private readonly Vector2 VIEW_PORT_RIGHT_TOP = UbhUtil.VECTOR2_ONE;

    [SerializeField, FormerlySerializedAs("_UseAxis")]
    private UbhUtil.AXIS m_useAxis = UbhUtil.AXIS.X_AND_Y;

    private UbhSpaceship m_spaceship;
    private UbhGameManager m_manager;
    private Transform m_backgroundTransform;
    private bool m_isTouch;
    private float m_lastXpos;
    private float m_lastYpos;
    private Vector2 m_tempVector2 = UbhUtil.VECTOR2_ZERO;
    private float m_focusFactor = 1f;
    private bool m_nearBlastZone = false;
    private float m_fallSpeedCache;

    private void Start()
    {
        m_spaceship = GetComponent<UbhSpaceship>();
        m_manager = FindObjectOfType<UbhGameManager>();
        m_backgroundTransform = FindObjectOfType<UbhBackground>().transform;
        SoundUtils.UpdateSFXVolume();
        m_fallSpeedCache = FallDown.s_fallSpeed;
    }

    private void MagnetPullTick()
    {
        Collider2D[] colliders;
        Rigidbody2D rigidbody;

        colliders = Physics2D.OverlapCircleAll(transform.position, 1.5f, LayerMask.GetMask("Item"));

        if (colliders.Length == 0)
        {
            FallDown.s_resetVelocity = true;
        }
        else
        {
            FallDown.s_resetVelocity = false;
        }

        foreach (Collider2D collider in colliders)
        {
            rigidbody = collider.GetComponent<Rigidbody2D>();
            rigidbody.velocity = Vector3.zero;

            if (rigidbody == null)
            {
                continue;
            }

            Vector3 position = Vector3.MoveTowards(rigidbody.position, transform.position, Time.smoothDeltaTime * 6f);
            rigidbody.position = position;
        }
    }


    private void Update()
    {

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            m_focusFactor = 0.5f;
        }
        else
        {
            m_focusFactor = 1f;
        }

        CheckNearBlastZone();
        MagnetPullTick();


        if (UbhUtil.IsMobilePlatform())
        {
            TouchMove();
#if UNITY_EDITOR
            KeyMove();
#endif
        }
        else
        {
            KeyMove();

            if (Input.GetKeyDown(KeyCode.B))
            {
                if (!GameObject.FindObjectOfType(typeof(BombGrow)))
                {
                    Transform t = PoolBoss.Spawn(m_bombPrefab.transform, transform.position, UbhUtil.QUATERNION_IDENTITY, transform);
                    t.SetLocalScale(1f, 1f, 1f);
                    t.gameObject.transform.parent = gameObject.transform;
                }
            }
        }

    }


    private void CheckNearBlastZone()
    {
        if (m_nearBlastZone)
        {
            GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
            foreach (GameObject go in items)
            {
                Rigidbody2D rigidbody = go.GetComponent<Rigidbody2D>();
                Vector3 position = Vector3.MoveTowards(rigidbody.position, transform.position, Time.smoothDeltaTime * 8f);
                rigidbody.position = position;
            }
        }
    }

    private void KeyMove()
    {
        m_tempVector2.x = Input.GetAxisRaw(AXIS_HORIZONTAL);
        m_tempVector2.y = Input.GetAxisRaw(AXIS_VERTICAL);
        Move(m_tempVector2.normalized);

        Camera main = DataCache.GetMainCamera();
        Vector3 vec = main.transform.position;

        var topRight = main.ScreenToWorldPoint(new Vector3(
            main.pixelWidth, main.pixelHeight));

        if (transform.position.y >= (topRight.y * .8f))
        {
            FallDown.s_fallSpeed = 0f;
            m_nearBlastZone = true;
        }
        else
        {
            FallDown.s_fallSpeed = m_fallSpeedCache;
            m_nearBlastZone = false;
        }
    }

    private void TouchMove()
    {
        float xPos = 0f;
        float yPos = 0f;
        if (Input.GetMouseButtonDown(0))
        {
            m_isTouch = true;
            Vector3 vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            xPos = vec.x;
            yPos = vec.y;
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 vec = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            xPos = vec.x;
            yPos = vec.y;
            if (m_isTouch)
            {
                m_tempVector2.x = (xPos - m_lastXpos) * 10f;
                m_tempVector2.y = (yPos - m_lastYpos) * 10f;
                Move(m_tempVector2.normalized);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_isTouch = false;
        }
        m_lastXpos = xPos;
        m_lastYpos = yPos;
    }

    private void Move(Vector2 direction)
    {
        Vector2 min;
        Vector2 max;
        if (m_manager != null && m_manager.m_scaleToFit)
        {
            min = Camera.main.ViewportToWorldPoint(VIEW_PORT_LEFT_BOTTOM);
            max = Camera.main.ViewportToWorldPoint(VIEW_PORT_RIGHT_TOP);
        }
        else
        {
            Vector2 scale = m_backgroundTransform.localScale;
            min = scale * -0.5f;
            max = scale * 0.5f;
        }

        Vector2 pos = transform.position;
        if (m_useAxis == UbhUtil.AXIS.X_AND_Z)
        {
            pos.y = transform.position.z;
        }

        pos += direction * (m_focusFactor * m_spaceship.m_speed) * UbhTimer.instance.deltaTime;

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        if (m_useAxis == UbhUtil.AXIS.X_AND_Z)
        {
            transform.SetPosition(pos.x, transform.position.y, pos.y);
        }
        else
        {
            transform.position = pos;
        }
    }
}
