﻿using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(UbhSpaceship))]
public class UbhEnemy : UbhMonoBehaviour
{
    public const string NAME_PLAYER = "Player";
    public const string NAME_PLAYER_BULLET = "PlayerBullet";

    private const string ANIM_DAMAGE_TRIGGER = "Damage";

    [SerializeField, FormerlySerializedAs("_UseStop")]
    private bool m_useStop = false;
    [SerializeField, FormerlySerializedAs("_StopPoint")]
    private float m_stopPoint = 2f;

    private UbhSpaceship m_spaceship;

    private void Start()
    {
        m_spaceship = GetComponent<UbhSpaceship>();

        Move(transform.up * -1);
    }

    private void FixedUpdate()
    {
        if (m_useStop)
        {
            if (transform.position.y < m_stopPoint)
            {
                rigidbody2D.velocity = UbhUtil.VECTOR2_ZERO;
                m_useStop = false;
            }
        }
    }

    public void Move(Vector2 direction)
    {
        rigidbody2D.velocity = direction * m_spaceship.m_speed;
    }
}