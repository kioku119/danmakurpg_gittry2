﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//public static class ReplayRecord : MonoBehaviour
public class ReplayRecord : MonoBehaviour
{
    /*private Dictionary<float, List<KeyCode>> pressDict = new Dictionary<float, List<KeyCode>>();
    private Dictionary<float, List<KeyCode>> releaseDict = new Dictionary<float, List<KeyCode>>();
    private List<Dictionary<float, List<KeyCode>>> replayList = new List<Dictionary<float, List<KeyCode>>>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        replayList = new List<Dictionary<float, List<KeyCode>>>();

        List<KeyCode> listPress = new List<KeyCode>(5);
        List<KeyCode> listRelease = new List<KeyCode>(5);
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) { listPress.Add(KeyCode.LeftArrow); }
            if (Input.GetKeyDown(KeyCode.RightArrow)) { listPress.Add(KeyCode.RightArrow); }
            if (Input.GetKeyDown(KeyCode.DownArrow)) { listPress.Add(KeyCode.DownArrow); }
            if (Input.GetKeyDown(KeyCode.UpArrow)) { listPress.Add(KeyCode.UpArrow); }

            pressDict.Add(Time.realtimeSinceStartup, listPress);
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (Input.GetKeyUp(KeyCode.LeftArrow)) { listRelease.Add(KeyCode.LeftArrow); }
            if (Input.GetKeyUp(KeyCode.RightArrow)) { listRelease.Add(KeyCode.RightArrow); }
            if (Input.GetKeyUp(KeyCode.DownArrow)) { listRelease.Add(KeyCode.DownArrow); }
            if (Input.GetKeyUp(KeyCode.UpArrow)) { listRelease.Add(KeyCode.UpArrow); }

            releaseDict.Add(Time.realtimeSinceStartup, listRelease);
        }
        replayList.Add(listPress);
        replayList.Add(listRelease);

    }

    public void SaveReplay(int num)
    {
        var bf = new BinaryFormatter();
        var savePath = Application.persistentDataPath + "/replay" + num.ToString() + ".dat";
        var fi = new System.IO.FileInfo(savePath);

        using (var binaryFile = fi.Create())
        {
            bf.Serialize(binaryFile, replayList);
            binaryFile.Flush();
        }
    }
    public void LoadReplay(int num)
    {
        var bf = new BinaryFormatter();
        var savePath = Application.persistentDataPath + "/replay" + num.ToString() + ".dat";
        var fi = new System.IO.FileInfo(savePath);

        Dictionary<float, List<KeyCode>> readBack;

        using (var binaryFile = fi.OpenRead())
        {
            readBack = (List<Dictionary<float, List<KeyCode>>>)bf.Deserialize(binaryFile);
        }
    }*/
}