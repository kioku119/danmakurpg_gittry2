﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class SAndMCurve : MonoBehaviour {
    public enum patternType
    {
        s, m, petal, straightLineSequence, circle, spiral
    }

    public int numCurves;
    public patternType pattern;
    public float heightDiff;
    public float heightDiffDelta;
    public float tightness;
    public float tightnessDelta;
    public float fractionToPeak = .5f;
    public float fractionToPeakDelta;
    //this is for circles and spirals. Can be -1 to 1. -1 inverts circle controls
    //and thus makes a quilt-y sort of shape.
    public float roundness = 1f;
    //Only for straight line sequence
    public float angle;
    public float deltaAngle;
    public bool isVertical = true;

    public float width;
    public bool isBezierSimetric = false;

    private BGCurve curve;
    private BGCcCollider2DBox boxCollider;
    private LineRenderer lineRenderer;

    private BGCurvePointI prevPoint;
    private int curCurveDirection = 1;

    private float circleControlDistance = (4.0f / 3.0f) * Mathf.Tan(Mathf.PI / 8.0f);

    private int spiralSegmentDirection = 0;
    private bool isFirstSpiralSegment = true;
    

    // Use this for initialization
    void Start () {
        Debug.Log("tan test " + circleControlDistance);
        if ((fractionToPeak > 1) || (fractionToPeak < 0))
        {
            throw new Exception("fraction to peak must be in the range 0 to 1!");
        }

        curve = gameObject.GetComponent<BGCurve>();

        boxCollider = gameObject.GetComponent<BGCcCollider2DBox>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();

        boxCollider.Height = width;
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;

        prevPoint = curve[0];

        BuildPattern();
	}

    void BuildPattern()
    {
        for(int i = 0; i < numCurves; ++i)
        {
            switch(pattern)
            {
                case patternType.s:
                    MakeSMCurve(true);
                    break;
                case patternType.m:
                    MakeSMCurve(false);
                    break;
                case patternType.petal:
                    MakePetalCurve();
                    break;
                case patternType.straightLineSequence:
                    MakeStraightLineSequence();
                    break;
                case patternType.circle:
                    MakeCircleCurve();
                    break;
                case patternType.spiral:
                    MakeSpiralCurve();
                    break;
            }
        }
    }

    void MakeSMCurve(bool isSCurve)
    {

        Vector3 newPos = prevPoint.PositionLocal;
        Vector3 control1 = Vector3.zero;

        if(isVertical)
        {
            newPos.y += heightDiff;
            control1.x = tightness;

            if(isSCurve)
                control1.x *= curCurveDirection;

            float pointDisplacement = (-1 * heightDiff) * (1 - fractionToPeak);
            control1.y = pointDisplacement;
        }
        else
        {
            newPos.x += heightDiff;
            control1.y = tightness;

            if (isSCurve)
                control1.y *= curCurveDirection;

            float pointDisplacement = (-1 * heightDiff) * (1 - fractionToPeak);
            control1.x = pointDisplacement;
        }

        BGCurvePoint newPoint;
        if(!isBezierSimetric)
            newPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierIndependant, control1, Vector3.zero);
        else
            newPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierIndependant, control1, -1 * control1);

        curve.AddPoint(newPoint);

        prevPoint = newPoint;
        if (isSCurve)
            curCurveDirection *= -1;
        heightDiff += heightDiffDelta;
        tightness += tightnessDelta;
        fractionToPeak += fractionToPeakDelta;

    }

    void MakePetalCurve()
    {
        Vector3 newPos = prevPoint.PositionLocal;
        Vector3 control1 = Vector3.zero;

        if (isVertical)
        {
            newPos.y += (heightDiff * curCurveDirection);
            control1.x = tightness * curCurveDirection;

            float pointDisplacement = (-1 * heightDiff * curCurveDirection) * (1 - fractionToPeak);
            control1.y = pointDisplacement;
        }
        else
        {
            newPos.x += (heightDiff * curCurveDirection);
            control1.y = tightness * curCurveDirection;

            float pointDisplacement = (-1 * heightDiff * curCurveDirection) * (1 - fractionToPeak);
            control1.x = pointDisplacement;
        }

        BGCurvePoint newPoint;
        if (!isBezierSimetric)
            newPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierIndependant, control1, Vector3.zero);
        else
            newPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierIndependant, control1, -1 * control1);

        curve.AddPoint(newPoint);

        prevPoint = newPoint;
        heightDiff += heightDiffDelta;
        tightness += tightnessDelta;
        fractionToPeak += fractionToPeakDelta;
        curCurveDirection *= -1;
    }

    void MakeCircleCurve()
    {
        if ((roundness > 1) || (roundness < -1))
        {
            throw new Exception("roundness must be in the range -1 to 1!");
        }

        float radius = .5f * heightDiff;
        float controlDist = circleControlDistance * Mathf.Abs(radius) * roundness;
        BGCurvePoint firstPoint;
        Vector3 newPos = Vector3.zero;
        Vector3 control = Vector3.zero;
        if (isVertical)
        {
            float controlx = controlDist;
            if (heightDiff > 0)
                controlx *= -1;

            control.x = controlx;
            firstPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve[0] = firstPoint;

            float controly = controlx;
            BGCurvePoint curPoint;
            control.x = 0;
            control.y = controly;
            newPos.x += radius;
            newPos.y += radius;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);

            controlx *= -1;
            control.x = controlx;
            control.y = 0;
            newPos.x = 0;
            newPos.y += radius;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);

            control.x = 0;
            control.y = controly *-1;
            newPos.y = radius;
            newPos.x = radius * -1;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);

        }
        else
        {
            float controly = controlDist;
            if (heightDiff > 0)
                controly *= -1;

            control.y = controly;
            firstPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve[0] = firstPoint;

            float controlx = controly;
            BGCurvePoint curPoint;
            control.y = 0;
            control.x = controlx;
            newPos.x += radius;
            newPos.y += radius;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);
            
            controly *= -1;
            control.y = controly;
            control.x = 0;
            newPos.y = 0;
            newPos.x += radius;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);
            
            control.y = 0;
            control.x = controlx * -1;
            newPos.x = radius;
            newPos.y = radius * -1;
            curPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierSymmetrical, control, -1 * control);
            curve.AddPoint(curPoint);
            
        }

        curve.AddPoint(firstPoint);
    }

    void MakeSpiralCurve()
    {
        //TODO: still wrong. Need half and half for in/out. independent. change then *-1.

        if ((roundness > 1) || (roundness < -1))
        {
            throw new Exception("roundness must be in the range -1 to 1!");
        }

        if (isFirstSpiralSegment && !isVertical)
            ++spiralSegmentDirection;

        float radius = .5f * heightDiff;
        float controlBase = circleControlDistance * roundness;
        float controlDist = controlBase * Mathf.Abs(radius);
        float nextControlDist = controlBase * Mathf.Abs(radius + (.5f * heightDiffDelta));

        BGCurvePoint newPoint;
        Vector3 newPos = Vector3.zero;
        Vector3 control1 = Vector3.zero;
        Vector3 control2 = Vector3.zero;

        if (spiralSegmentDirection == 0)
        {
            newPos.y = -1 * radius;
            if (heightDiff > 0)
            {
                controlDist *= -1;
                nextControlDist *= -1;
            }

            control1.x = controlDist;
            control2.x = nextControlDist;
        }
        else if (spiralSegmentDirection == 1)
        {
            newPos.x = 1 * radius;
            if (heightDiff > 0)
            {
                controlDist *= -1;
                nextControlDist *= -1;
            }
            control1.y = controlDist;
            control2.y = nextControlDist;
        }
        else if (spiralSegmentDirection == 2)
        {
            newPos.y = 1 * radius;
            if (heightDiff < 0)
            {
                controlDist *= -1;
                nextControlDist *= -1;
            }

            control1.x = controlDist;
            control2.x = nextControlDist;
        }
        else
        {
            newPos.x = -1 * radius;
            if (heightDiff < 0)
            {
                controlDist *= -1;
                nextControlDist *= -1;
            }

            control1.y = controlDist;
            control2.y = nextControlDist;
        }

        newPoint = new BGCurvePoint(curve, newPos, BGCurvePoint.ControlTypeEnum.BezierIndependant, control1, -1 * control2);
        if (isFirstSpiralSegment)
        {
            isFirstSpiralSegment = false;
            curve[0] = newPoint;
        }
        else
            curve.AddPoint(newPoint);

        heightDiff += heightDiffDelta;
        ++spiralSegmentDirection;
        if (spiralSegmentDirection > 3)
            spiralSegmentDirection = 0;
    }

    void MakeStraightLineSequence()
    {

    }

    // Update is called once per frame
    void Update () {
		
	}
}
